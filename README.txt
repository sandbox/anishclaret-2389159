Douban is a sub-module of Chinese Social Networking Authentication.
It provides an option to user to login with his Douban login credentials.

Basic Instructions:
1. Download Chinese Social Networks Authentication (CSNA).
2. Download the Douban CSNA from sandbox mode.
3. Creating a new douban application from douban application page.
Provide application name, description, web application url and callback
url(Optional).
select "Web Application" from Application type, agree to the
terms and conditions and click on "Create an application".
4. Once the application is created, Navigate to My application page.
Edit the application and provide necessary permissions by clicking on the
 "api permissions" link on the left sidebar.
 Note: Alleast one api permission is required.
5. Navigate to the "Submitted for review" tab from left side navigation
 and submit the app for review from douban team.
 It will take around 24 hrs for the app get reviewed.
6. Once the application got approved, we need to copy the "Application key"
 and "Secret" to douban configuration page('admin/config/services/csna') in
 your website. Douban login link will appear on your user login page along with
 other enabled csna sub-modules. Your can get more information from the CSNA
 page

le
 * Admin settings page for the douban module.
 */

/**
 * Setting API key and secret key at settings page.
 */
function douban_admin_settings() {

  $form = array();
  $form['douban_consumer_key'] = array(
    '#type' => 'textfield',
    '#title' => t('API key'),
    '#default_value' => variable_get('douban_consumer_key', ''),
    '#description' => t('Consumer key from your application settings page.'),
  );
  $form['douban_consumer_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('Secret Key'),
    '#default_value' => variable_get('douban_consumer_secret', ''),
    '#description' => t('Consumer secret from your application settings page.'),
  );

  //return system_settings_form($form);
}

function douban_admin_proxy() {

$form = array();
  $form['douban_proxy_port'] = array(
    '#type' => 'textfield',
    '#title' => t('Proxy Port'),
    '#default_value' => variable_get('douban_proxy_port', ''),
    '#description' => t('Please specify the port number'),
  );
  $form['douban_proxy_type'] = array(
    '#type' => 'textfield',
    '#title' => t('Proxy Type'),
    '#default_value' => variable_get('douban_proxy_type', ''),
    '#description' => t('Please specify the port type. e.g http , https'),
  );
  $form['douban_proxy'] = array(
    '#type' => 'textfield',
    '#title' => t('Proxy server'),
    '#default_value' => variable_get('douban_proxy', ''),
    '#description' => t('Please enter the proxy proxy server address'),
  );
  $form['douban_proxy_username_password'] = array(
    '#type' => 'textfield',
    '#title' => t('Proxy server authentication'),
    '#default_value' => variable_get('douban_proxy_username_password', ''),
    '#description' => t('Username and password for the proxy server. It should be in the following format username:password'),
  );

  return system_settings_form($form);

}
